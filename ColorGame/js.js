var numOfSquears = 6;
var colors = addColorsToArray(numOfSquears);

var squears = document.querySelectorAll(".square");
var pickedColor = rndColor();
var colorDisplay = document.getElementById("selectedColor");
var h1 = document.querySelector("h1");
colorDisplay.textContent = pickedColor;

var result = document.querySelector("#displayresult");
var reset = document.querySelector("#reset");

var modeButtons = document.querySelectorAll(".modebtn");

init();

function init(){
   setUpButtons();
   setUpSquears();

}



function changeColors(color){
	for(var i = 0; i < squears.length; i++)
	{
		squears[i].style.backgroundColor = color;
	}
}

function rndColor(){
	var random = Math.floor(Math.random() * colors.length);
	return colors[random];
}

function addColorsToArray(num){
	var arr = []
	for(var i = 0; i < num; i++){
		arr.push(randomColor());
	}
	return arr;
}

function randomColor(){
	 var r = Math.floor(Math.random() * 256);
	 var g = Math.floor(Math.random() * 256);
	 var b = Math.floor(Math.random() * 256);

	 return "rgb("+r+", "+g+", "+b+")";
}

function resetPage(){
    colors = addColorsToArray(numOfSquears);
	//pick a new colortoguess
	pickedColor = rndColor();
	//change colors of all squears
	for(var i = 0; i < 6; i++)
	{
		if(colors[i]){
			squears[i].style.display = "inline-block";
		squears[i].style.backgroundColor = colors[i];
	   }
        else{
        	squears[i].style.display = "none";
        }
	}
	//display new colortoguess
	colorDisplay.textContent = pickedColor;
	//reset h1 color
	h1.style.backgroundColor = "steelblue";
	reset.textContent = "New Colors";
	result.textContent = "";
}

function setUpButtons(){
	reset.addEventListener("click", function(){
	resetPage();
})
		for(var i = 0; i < modeButtons.length; i++){
	modeButtons[i].addEventListener("click", function(){
		modeButtons[0].classList.remove("selected");
		modeButtons[1].classList.remove("selected");
		this.classList.add("selected");
		this.textContent === "Easy"? numOfSquears = 3: numOfSquears = 6;
		resetPage();
	})
  }
}
function setUpSquears(){
	  for(var i = 0; i < colors.length; i++)
{
	// add colors to squears
  squears[i].style.backgroundColor = colors[i];
  // add eventlisteners
  squears[i].addEventListener("click", function(){
  	var picked = this.style.backgroundColor;
  	if(picked === pickedColor){
       changeColors(picked);
  	   result.textContent = "Correct";	
  	   h1.style.backgroundColor = picked;
  	   reset.textContent = "Play Again?";
  	}
  	else{
  		this.style.backgroundColor = "#232323";
  		result.textContent = "Try again";
  	}

  	
  })
}
}